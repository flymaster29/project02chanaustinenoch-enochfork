﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion_Field : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Destroy (this.gameObject, 1f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnTriggerEnter(Collider other){
		if (other.CompareTag ("Enemy")) {
			Destroy (other.gameObject);
		}
	}
}
