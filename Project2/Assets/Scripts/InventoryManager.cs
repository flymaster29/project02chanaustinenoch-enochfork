﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InventoryManager : MonoBehaviour {


	public static InventoryManager instance = null;
	private List<GameObject> items = new List<GameObject>();
	public GameObject selectedItem;
	public string selectedItemType;
	void Awake() {
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (this.gameObject);
		}
		//DontDestroyOnLoad (this.gameObject);
	}
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		CheckForInput ();
	}

	void CheckForInput() {
		if (Input.GetKeyDown(KeyCode.F1)) {
			Debug.Log ("f1");
			if (Ui_Controller.instance.InventoryAreaHolders [0].sprite.name != "Background") {
				Ui_Controller.instance.SetActiveInventoryItem (0);
				selectedItemType = Ui_Controller.instance.InventoryAreaHolders [0].sprite.name;
				//selectedItem = GetObjectOfTypeName ();
			} 

		}
		if (Input.GetKeyDown(KeyCode.F2)) {
			Debug.Log ("f2");

			if (Ui_Controller.instance.InventoryAreaHolders [1].sprite.name != "Background") {
				Ui_Controller.instance.SetActiveInventoryItem (1);
				selectedItemType = Ui_Controller.instance.InventoryAreaHolders [1].sprite.name;
			} else {
				selectedItemType = null;
			}

		}
		if (Input.GetKeyDown(KeyCode.F3)) {
			Debug.Log ("f3");

			if (Ui_Controller.instance.InventoryAreaHolders [2].sprite.name != "Background") {
				Ui_Controller.instance.SetActiveInventoryItem (2);
				selectedItemType = Ui_Controller.instance.InventoryAreaHolders [2].sprite.name;
			} 

		}
		if (Input.GetKeyDown(KeyCode.F4)) {
			Debug.Log ("f4");

			if (Ui_Controller.instance.InventoryAreaHolders [3].sprite.name != "Background") {
				Ui_Controller.instance.SetActiveInventoryItem (3);
				selectedItemType = Ui_Controller.instance.InventoryAreaHolders [3].sprite.name;
			} else {
				selectedItemType = null;
			}

		}
		if (Input.GetKeyDown(KeyCode.F5)) {
			Debug.Log ("f5");

			if (Ui_Controller.instance.InventoryAreaHolders [4].sprite.name != "Background") {
				Ui_Controller.instance.SetActiveInventoryItem (4);
				selectedItemType = Ui_Controller.instance.InventoryAreaHolders [4].sprite.name;
			} 

		}
		if (Input.GetKeyDown(KeyCode.F6)) {
			Debug.Log ("f6");

			if (Ui_Controller.instance.InventoryAreaHolders [5].sprite.name != "Background") {
				Ui_Controller.instance.SetActiveInventoryItem (5);
				selectedItemType = Ui_Controller.instance.InventoryAreaHolders [5].sprite.name;
			}

		}
		if (Input.GetKeyDown(KeyCode.F7)) {
			Debug.Log ("f7");

			if (Ui_Controller.instance.InventoryAreaHolders [6].sprite.name != "Background") {
				Ui_Controller.instance.SetActiveInventoryItem (6);
				selectedItemType = Ui_Controller.instance.InventoryAreaHolders [6].sprite.name;
			}

		}
		if (Input.GetKeyDown(KeyCode.F8)) {
			Debug.Log ("f8");

			if (Ui_Controller.instance.InventoryAreaHolders [7].sprite.name != "Background") {
				Ui_Controller.instance.SetActiveInventoryItem (7);
				selectedItemType = Ui_Controller.instance.InventoryAreaHolders [7].sprite.name;
			}

		}
		if (Input.GetKeyDown(KeyCode.F9)) {
			Debug.Log ("f9");

			if (Ui_Controller.instance.InventoryAreaHolders [8].sprite.name != "Background") {
				Ui_Controller.instance.SetActiveInventoryItem (8);
				selectedItemType = Ui_Controller.instance.InventoryAreaHolders [8].sprite.name;
			}

		}
		if (Input.GetKeyDown(KeyCode.F10)) {
			Debug.Log ("f10");

			if (Ui_Controller.instance.InventoryAreaHolders [9].sprite.name != "Background") {
				Ui_Controller.instance.SetActiveInventoryItem (9);
				selectedItemType = Ui_Controller.instance.InventoryAreaHolders [9].sprite.name;
			}

		}
		if (Input.GetKeyDown(KeyCode.H)) {
			Debug.Log ("H");
			RemoveItem (selectedItemType);
		}
	}
	public void AddItem(GameObject item, bool stackable = true) {
		items.Add (item);
		int tempCount = 0;
		bool doSecondLoop = true;
		if (stackable) {
			foreach (Image element in Ui_Controller.instance.InventoryAreaHolders) {
				if (element.GetComponent<Image> ().sprite.name == item.GetComponent<Item> ().itemImageSprite.name) {
					Ui_Controller.instance.InventoryCountHolders [tempCount].text = "" + HowManyItemOfThisType (item.GetComponent<Item> ().typeOfItem);
					doSecondLoop = false;
				}
				tempCount++;
			}
		}
		if (doSecondLoop) {
			tempCount = 0;
			foreach (Image element in Ui_Controller.instance.InventoryAreaHolders) {
				if (element.GetComponent<Image> ().sprite.name == "Background") {
					Ui_Controller.instance.InventoryAreaHolders [tempCount].sprite = item.GetComponent<Item> ().itemImageSprite;
					Ui_Controller.instance.InventoryCountHolders [tempCount].text = "" + HowManyItemOfThisType (item.GetComponent<Item> ().typeOfItem);
					break;
				} 
				tempCount++;
			}
		}
		item.SetActive (false);
	}

	

	public void RemoveItem(string type) {
		GameObject item = GetObjectOfTypeName (type);
		items.Remove (item);
		if (item != null) {
			SpawnDroppedObject.instance.SpawnObject (item);

			int tempCount = 0;
			foreach (Image element in Ui_Controller.instance.InventoryAreaHolders) {
				if (element.GetComponent<Image> ().sprite.name == item.GetComponent<Item> ().itemImageSprite.name) {
					if (HowManyItemOfThisType (item.GetComponent<Item> ().typeOfItem) <= 0) {
						Ui_Controller.instance.InventoryAreaHolders [tempCount].sprite = Ui_Controller.instance.itemTypeImageSources [2];
						Ui_Controller.instance.ResetInventoryPlaceholderColors ();
						selectedItemType = "";
						//selectedItemType = null;

					}
					Ui_Controller.instance.InventoryCountHolders [tempCount].text = "" + HowManyItemOfThisType (item.GetComponent<Item> ().typeOfItem);

					break;


				}
				tempCount++;
			}
		}
			


	}

	public int HowManyItemOfThisType(Item.ItemType type) {
		int tempCount = 0;
		foreach(GameObject x in items) {
			if (x.GetComponent<Item> ().typeOfItem == type) {
				tempCount++;
			}
		}
		return tempCount;
	}

	public GameObject GetObjectOfTypeName(string name) {
		foreach (GameObject element in items) {
			if (element.GetComponent<Item> ().itemImageSprite.name == name) {
				return element.gameObject;
			}
		}
		selectedItemType = null;
		return null;
	}

	public bool ItemExistInList(GameObject item) {
		bool foundOne = false;
		foreach (GameObject element in items) {
			Debug.Log ("items: " + items);
			if (item.GetComponent<Item> ().itemImageSprite.name == element.GetComponent<Item> ().itemImageSprite.name) {
				Debug.Log ("Got Here:");
				foundOne = true;
			} 
		}

		return foundOne;
	}
}
