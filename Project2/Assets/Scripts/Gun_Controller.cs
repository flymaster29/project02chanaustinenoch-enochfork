﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun_Controller : MonoBehaviour
{
	public MonoBehaviour handgunScript;
	public MonoBehaviour assaultScript;
	public MonoBehaviour shotgunScript;
	public MonoBehaviour bazookaScript;

    public bool assaultAccess;
	public bool shotgunAccess;
	public bool bazookaAccess;

    // Use this for initialization
    void Start () {
		handgunScript = (MonoBehaviour)gameObject.GetComponent ("Gun_BasicHandgun");
		assaultScript = (MonoBehaviour)gameObject.GetComponent ("Gun_AssaultRifle");
		shotgunScript = (MonoBehaviour)gameObject.GetComponent ("Gun_Shotgun");
		bazookaScript = (MonoBehaviour)gameObject.GetComponent ("Gun_Bazooka");
	}
	
	// Update is called once per frame
	void Update ()
    {
		CheckForHotBar ();
	}

	public void CheckForHotBar(){
		if (Input.GetKeyDown(KeyCode.Alpha1)) {
			Debug.Log ("Key1");
			handgunScript.enabled = true;
			assaultScript.enabled = false;
			shotgunScript.enabled = false;
			bazookaScript.enabled = false;
		}
		if (Input.GetKeyDown(KeyCode.Alpha2) && assaultAccess) {
			Debug.Log ("Key2");
			handgunScript.enabled = false;
			assaultScript.enabled = true;
			shotgunScript.enabled = false;
			bazookaScript.enabled = false;
        }
		if (Input.GetKeyDown(KeyCode.Alpha3) &&shotgunAccess) {
			Debug.Log ("Key3");
			handgunScript.enabled = false;
			assaultScript.enabled = false;
			shotgunScript.enabled = true;
			bazookaScript.enabled = false;
		}
		if (Input.GetKeyDown(KeyCode.Alpha4) && bazookaAccess) {
			Debug.Log ("Key4");
			handgunScript.enabled = false;
			assaultScript.enabled = false;
			shotgunScript.enabled = false;
			bazookaScript.enabled = true;
		}
	}
}
