﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ui_Controller : MonoBehaviour
{
    
	public static Ui_Controller instance = null;
	public Sprite[] itemTypeImageSources;
	public Text Lives;
    public Text health;
    public Text Timer;
    public Text Ammo;
	public Image[] InventoryAreaHolders;
	public Text[] InventoryCountHolders;
	public GameObject selectedItem;

	void Awake() {
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (this.gameObject);
		}
		//DontDestroyOnLoad (this.gameObject);
	}
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        UpdateHealthUI();
        UpdateLivesUI();
    }

    public void UpdateHealthUI()
    {
        health.text = "Player Health: " + GameManager.instance.playerHealth.ToString();
    }

    public void UpdateLivesUI()
    {
        Lives.text = "Player Lives: " + GameManager.instance.playerLives.ToString();
    }

    public void UpdateTimer(string update)
    {
        Timer.text = update;
    }

    public void UpdateAmmo(string update)
    {
        Ammo.text = update;
    }

	public void SetActiveInventoryItem(int index) {
		ResetInventoryPlaceholderColors ();
		InventoryAreaHolders [index].transform.parent.GetComponent<Image>().color = Color.black;
		Debug.Log("GameObject Image Source Name: " + InventoryAreaHolders [index].transform.parent.GetComponent<Image>().sprite.name);
	}
		

	public void ResetInventoryPlaceholderColors() {
		foreach (Image element in InventoryAreaHolders) {
			element.transform.transform.parent.GetComponent<Image> ().color = Color.white;
		}
	}
}
