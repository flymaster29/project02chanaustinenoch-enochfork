﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun_Aim : MonoBehaviour
{
    public Vector3 aimPostion;
    private Vector3 initialPosition;
    public float smooth = 1000;

    // Use this for initialization
    void Start ()
    {
        initialPosition = gameObject.transform.localPosition;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetMouseButton(1))
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition,
                aimPostion, Time.deltaTime * smooth);
        }
        else
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition,
                initialPosition, Time.deltaTime * smooth);
        }
	}
}
