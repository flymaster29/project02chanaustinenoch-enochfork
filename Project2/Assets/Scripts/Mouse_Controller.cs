﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Mouse_Controller.cs control all the mouse movement and Cursor locking 
/// </summary>
public class Mouse_Controller : MonoBehaviour
{
    [Tooltip("Mouse sensitivity for both X and Y movement.")]
    public float mouseSensitivity = 2f;
    [Tooltip("Angle to rotation the camera view up/down in degrees.")]
    public float verticalLimit = 90f;

    private Camera mainCam;

    public bool lockState = true;
    // Use this for initialization
    void Start ()
    {
        mainCam = Camera.main;
    }
	
	// Update is called once per frame
	void Update ()
    {
        RotateView();
        SetCursorState();
    }

    /// <summary>
    /// calculates the movment for the main camera when the mosue is moving 
    /// </summary>
    void RotateView()
    {
     //////if we need a pause add check here

         // mouse lock based on the first person

         // Get mouse inputs
         float yRot = Input.GetAxis("Mouse X") * mouseSensitivity;
         float xRot = Input.GetAxis("Mouse Y") * mouseSensitivity;

         // Clamp x axis rotation (looking up and down)
         Quaternion qMainCam = mainCam.transform.localRotation;
         qMainCam *= Quaternion.Euler(-xRot, 0f, 0f);
         qMainCam.x /= qMainCam.w;
         qMainCam.y /= qMainCam.w;
         qMainCam.z /= qMainCam.w;
         qMainCam.w = 1.0f;
         float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(qMainCam.x);
         angleX = Mathf.Clamp(angleX, -verticalLimit, verticalLimit);
         qMainCam.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

         // Update player and Main Camera transforms
         transform.localRotation *= Quaternion.Euler(0f, yRot, 0f);
         mainCam.transform.localRotation = qMainCam;
    }

    /// <summary>
    /// responsible for the cursor being locked
    /// </summary>
    void SetCursorState()
    {
    ///// if pasued lockState should equal false
        if (lockState == false)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }
}
