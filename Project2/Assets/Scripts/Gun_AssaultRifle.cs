﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun_AssaultRifle : MonoBehaviour {

	public float cooldownTime;
	public int maxAmmo;
	public int ammoCount;
	public GameObject assaultBullet;
	float currentCooldown;
    public AudioClip assaultSound;

    public Ui_Controller uiController;
    // Use this for initialization
    void Start () 
	{

	}

	// Update is called once per frame
	void Update () 
	{
        uiController.UpdateAmmo("Ammo: " + ammoCount + "/" + maxAmmo);
        CheckForFireButton ();
		currentCooldown = Mathf.Max ((currentCooldown - (1 * Time.deltaTime)), 0);
		//Debug.Log (currentCooldown);
	}
	public void CheckForFireButton()
	{
		if (Input.GetMouseButton (0) && CheckForCooldown() && ammoCount > 0) 
		{
            AudioManager.instance.PlaySoundEffect(assaultSound);
			Debug.Log ("Fire");
			Instantiate (assaultBullet, transform.position, transform.rotation);
			ammoCount--;
			currentCooldown = cooldownTime;
		}
	}

	public bool CheckForCooldown()
	{
		if (currentCooldown <= 0) 
		{
			currentCooldown = cooldownTime;
			return true;
		} 
		else 
		{
			return false;
		}
	}
}
