﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun_Shotgun : MonoBehaviour {

	public float cooldownTime;
	public int maxAmmo;
	public int ammoCount;
	public int bulletsPerShot;
	public float spreadInDegree;
	public GameObject shotgunBullet;
	float currentCooldown;
    public AudioClip shotgunSound;

    public Ui_Controller uiController;
    // Use this for initialization
    void Start () 
	{

	}

	// Update is called once per frame
	void Update () 
	{
        uiController.UpdateAmmo("Ammo: " + ammoCount + "/" + maxAmmo);
        CheckForFireButton ();
		currentCooldown = Mathf.Max ((currentCooldown - (1 * Time.deltaTime)), 0);
		//Debug.Log (currentCooldown);
	}
	public void CheckForFireButton()
	{
		if (Input.GetMouseButtonDown (0) && CheckForCooldown() && ammoCount > 0) 
		{
            AudioManager.instance.PlaySoundEffect(shotgunSound);
			Debug.Log ("Fire");
			for (int i = 0; i < bulletsPerShot; i++) {
				Debug.Log ("Made");
				float randomAngle = Random.Range (-spreadInDegree, spreadInDegree);
				Instantiate (shotgunBullet, transform.position, transform.rotation);
			}
			ammoCount--;
            
            currentCooldown = cooldownTime;
		}
	}

	public bool CheckForCooldown()
	{
		if (currentCooldown <= 0) 
		{
			currentCooldown = cooldownTime;
			return true;
		} 
		else 
		{
			return false;
		}
	}
}
