﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// control all the player movement 
/// </summary>
public class Player_Movement : MonoBehaviour
{
    public float playerSpeed;
    public float jumpSpeed = 10f;
    public bool cruch;
    public float smooth = 100.0f;
    private float currentJumpForce;
    private bool jumped;
    private float hangtimer;
    private bool inAir;
    private CharacterController charCont;
    private Vector3 inputVector = Vector3.zero;
    private Vector3 moveDirection = Vector3.zero;
    private Vector3 initial;
    private Vector3 crouch;

    // Use this for initialization
    void Start ()
    {
        charCont = GetComponent<CharacterController>();
        initial = new Vector3(1,1,1);
        crouch = new Vector3(1, .75f, 1);
    }
	
	// Update is called once per frame
	void Update ()
    {
        MovePlayer();
    }

    /// <summary>
    /// resposible for controlling all the player movement and action
    /// </summary>
    void MovePlayer()
    {
        //gravity so it comes by down after junping
        inAir = !charCont.isGrounded;
        if (!inAir)
        {
            moveDirection.y = 0;

            // Get a 2d directional force from the vertical and horizontal axis inputs
            inputVector = transform.forward * Input.GetAxis("Vertical") + transform.right * Input.GetAxis("Horizontal");

            hangtimer = Time.time;
            jumped = false;

        }

        //crouch
        if (Input.GetKey(KeyCode.C))
        {
            transform.localScale = Vector3.Lerp(transform.localScale,
                crouch, Time.deltaTime * smooth);
        }
        else
        {
            transform.localScale = Vector3.Lerp(transform.localScale,
              initial, Time.deltaTime * smooth);
        }



        // Jump
        if (!jumped && (Time.time - hangtimer) < 0.5f)
        {

            if (Input.GetKeyDown(KeyCode.Space))
            {
                moveDirection.y = jumpSpeed;
                jumped = true;
            }

        }
        else
        {
            jumped = true;
        }


        // Apply gravity
        moveDirection += Physics.gravity * Time.deltaTime * 2;

        // Apply input speed to movement direction
        moveDirection.z = inputVector.z * playerSpeed;
        moveDirection.x = inputVector.x * playerSpeed;

        // Move the player
        charCont.Move(moveDirection * Time.deltaTime);

    }

}
