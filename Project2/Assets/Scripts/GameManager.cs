﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public int playerHealth;
    public int playerLives = 3;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    void Start ()
    {
		
	}

	void Update ()
    {
		if(playerHealth <= 0)
        {
            playerLives--;
            //restart current level?
        }
        if(playerLives <= 0 )
        {
            //restart game
        }
	}

    public void TakeHealth(int amount)
    {
        playerHealth -= amount;
    }

    public void GiveHealth(int amount)
    {
        playerHealth += amount;
    }

    public void TakeLives(int amount)
    {
        playerLives -= amount;
    }
}
